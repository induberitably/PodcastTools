use rayon::prelude::*;
use regex::Regex;
use std::fs;
use std::fs::File;
use std::io;
use std::io::Read;
use std::process::{Command, Stdio};
use std::thread;
use std::time::{Duration, Instant};
use std::{env, panic};

fn get_time_sequence_as_file(my_decible: String, my_audio_file: &String) {
    let my_audio_file_dash_i_win = format!("-i .\\{}", my_audio_file);
    let my_audio_file_dash_i_unix = format!("-i {}", my_audio_file);
    let af = format!("-af");
    let silence_detect = format!(
        "silencedetect=n=-{}dB:d=0.5,ametadata=print:file=log.txt",
        my_decible
    );
    let f = format!("-f");
    let str_null = format!("null");
    let dash = format!("-");
    //ffmpeg -i .\JustinGodLike.flac -af silencedetect=n=-50dB:d=0.5,ametadata=print:file=log.txt -f null -
    let my_cmd_win = format!(
        " ffmpeg {} {} {} {} {} {} ... 2>NUL",
        my_audio_file_dash_i_win, af, silence_detect, f, str_null, dash
    );
    //`
    let my_cmd_unix = format!(
        " ffmpeg {} {} {} {} {} {}",
        my_audio_file_dash_i_unix, af, silence_detect, f, str_null, dash
    );
    if cfg!(target_os = "windows") {
        let mut ffmpeg = Command::new("cmd");
        ffmpeg
            .args(&["/C", &my_cmd_win])
            .stdout(Stdio::null())
            .spawn()
            .expect("failed to execute process");
    }
    //TO-DO linux support
    else {
        println!(">>>>>>>>>>>>{}", my_cmd_unix);
        let mut ffmpeg = Command::new("sh");
        ffmpeg
            .arg("-c")
            .arg(my_cmd_unix)
            .stdout(Stdio::piped())
            .spawn()
            .expect("failed to execute process");
        println!("{:?}", ffmpeg)
    };
}
fn get_silence_time_from_file() -> (Vec<f32>, Vec<f32>) {
    let mut file = File::open("log.txt").unwrap();
    let mut contents: Vec<u8> = Vec::new();

    let result = file.read_to_end(&mut contents).unwrap();
    println!("Read {} bytes", result);
    let filestr = String::from_utf8(contents).unwrap();
    let re_start = Regex::new(r"(start)(.*)").unwrap();
    let re_end = Regex::new(r"(end)(.*)").unwrap();
    let re_eql_start = Regex::new(r"start=").unwrap();
    let re_eql_end = Regex::new(r"end=").unwrap();
    let mut start = Vec::new();
    let mut end = Vec::new();
    for caps in re_start.captures_iter(&filestr) {
        let k = re_eql_start.replace_all(caps.get(0).unwrap().as_str(), "");
        let t: f32 = k.as_ref().to_owned().parse().unwrap();

        start.push(t)
    }
    for caps in re_end.captures_iter(&filestr) {
        let k = re_eql_end.replace_all(caps.get(0).unwrap().as_str(), "");
        let t: f32 = k.as_ref().to_owned().parse().unwrap();
        end.push(t);
    }
    //remove the last starting silencing timestamp
    start.pop();
    println!("{} vs {}", start.len(), end.len());
    (start, end)
}
fn create_voice_at(name: String, timeTup: (Vec<f32>, Vec<f32>)) -> Vec<Command> {
    let startvec = timeTup.0;
    let endvec = timeTup.1;
    let v: Vec<&str> = name.split('.').collect();
    //let mut v_cmd: Vec<String> = Vec::new();
    let mut v_cmd: Vec<Command> = Vec::new();
    for i in 0..startvec.len() {
        if i < startvec.len() - 1 {
            let start_dur = startvec[i + 1];
            let end_dur = endvec[i];

            let wav_name = format!("{}-{}.wav", v[0], endvec[i]);
            //println!("Time: {:?},{:?}", start_dur, end_dur);
            //"-i .\\JustinGodLike.flac -ss 8.88585 -to 9.13856 -ac 1 C:\\Users\\Greenbeans\\source\\repos\\GamerVoice\\bin\\Debug\\netcoreapp3.1\\3JustinGodLike-00-00-08.8858500.wav -threads 4"	string

            let make_audio = format!(
                " -i {} -ss {} -to {} -ac 1 {} -threads 4 ... 2>NUL",
                name, end_dur, start_dur, wav_name
            );
            let make_audio_linux = format!(
                " -i {} -ss {} -to {} -ac 1 {} ",
                name, end_dur, start_dur, wav_name
            );

            let my_cmd_win = format!(" ffmpeg {}", make_audio);
            let my_cmd_linux = format!(" ffmpeg {}", make_audio_linux);
            if cfg!(target_os = "windows") {
                let mut ffmpeg = Command::new("cmd");
                ffmpeg.args(&["/C", &my_cmd_win]);
                // .spawn()
                //.expect("failed to execute process");

                v_cmd.push(ffmpeg);
            }
            //ffmpeg  -i JustinGodLike.flac -ss 174.159 -to 174.911 -ac 1 JustinGodLike-174.159.wav
            else {
                println!("<<<<<<<<<<{:?}", my_cmd_win);
                let mut ffmpeg = Command::new("sh");
                ffmpeg
                    .arg("-c")
                    .arg(my_cmd_linux)
                    .stdout(Stdio::piped())
                    .spawn()
                    .expect("failed to execute process");
                println!("{:?}", ffmpeg);

                v_cmd.push(ffmpeg);
            }
        }
    }
    println!("LEN-{}", v_cmd.len());
    v_cmd
}

fn main() {
    let now = Instant::now();
    println!("START: {}", now.elapsed().as_secs());
    let mut args: Vec<String> = env::args().collect();
    if args.len() != 3 {
        panic!("You need 2 args [audiofile, hz], you gave - {:?}" ,args.len()-1)
    }
    let my_decible = args.pop().unwrap();
    let my_audio_file = args.pop().unwrap();
    
    
    
    println!("{},{}", my_audio_file, my_decible);
    get_time_sequence_as_file(my_decible, &my_audio_file);
    let time_tup = get_silence_time_from_file();
    let mut v_cmd = create_voice_at(my_audio_file, time_tup);
    println!("%%{}", v_cmd.len());
    v_cmd.par_iter_mut().for_each(|p| {
        p.stdout(Stdio::null())
            .spawn()
            .expect("failed to execute process");
    });
    println!("END: {}", now.elapsed().as_secs());
}
